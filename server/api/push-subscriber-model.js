var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var PushSubscriber = new Schema({
	endpoint : {
		type : String,
		required : true,
		unique : true
	},
	registration_id : {
		type : String,
		required : true,
		unique : true
	},
	type : {
		type : String,
		required : true
	},
	key : {
		type : Schema.Types.Mixed,
		required : true
	},
	auth : {
		type : Schema.Types.Mixed,
		required : true
	}
});

module.exports = mongoose.model('pushsubscriber', PushSubscriber);