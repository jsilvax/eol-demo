var PushSubscriber = require('./push-subscriber-model'),
	PushNotifRouter = require('express').Router(),
	config = require('../config/config'),
	url = require('url'),
	https = require('https');

// Encryption
var crypto = require('crypto'),
	ece = require("http_ece"),
	urlBase64 = require("urlsafe-base64");

/**
 * @function encrypt
 * Based on https://github.com/marco-c/web-push
 * Encrypts a payload
 * @return Object
 */
function encrypt(userPublicKey, userAuth, payload) {
  if (typeof payload === 'string' || payload instanceof String) {
    payload = new Buffer(payload);
  }

  var localCurve = crypto.createECDH('prime256v1');
  var localPublicKey = localCurve.generateKeys();
  var salt = urlBase64.encode(crypto.randomBytes(16));

  ece.saveKey('webpushKey', localCurve, 'P-256');

  var conf = {
    keyid: 'webpushKey',
    dh: userPublicKey,
    salt: salt,
    authSecret : userAuth,
    padSize: 2  	
  };

  var cipherText = ece.encrypt(payload, conf);

  return {
    localPublicKey: urlBase64.encode(localPublicKey),
    salt: salt,
    cipherText: cipherText
  };
};

/**
 * @function pushToSubscriber
 * Will make the post request to the subscriber endpoint with the payload
 * @return Promise
 */

function pushToSubscriber(subscriber, payload){
	return new Promise(function(resolve, reject){
		// PUSH requires encryption for payloads
	  	var encrypted = encrypt(subscriber.key, subscriber.auth, payload);

	  	var urlParts = url.parse(subscriber.endpoint);

	  	var options = {
	 		hostname: urlParts.hostname,
	        port: urlParts.port,
	        path: urlParts.pathname,
	        method: 'POST',
	  		headers : {
				"Content-Type" : "application/octet-stream",
				"Encryption": 'keyid=p256dh;salt=' + encrypted.salt,
				"Crypto-Key" : "keyid=p256dh;dh=" +  encrypted.localPublicKey,
				"Content-Length" : encrypted.cipherText.length,
				"Content-Encoding" : 'aesgcm',
				"TTL" : 2419200 // Default the TTL to four weeks
	  		}
	  	};
	  	// Chrome requires us to pass along the GCM key
	  	if(subscriber.type === "chrome"){
			options.headers["Authorization"] = "key="+config.gcm;
	  	}

	    var request = https.request(options, function(res) {
		      if (res.statusCode === 200 || res.statusCode === 201) {
		           resolve(); // The user was notified
		      }
		      else if(subscriber.type === "chrome" && (res.statusCode === 400 || res.statusCode === 401)){
		      		// GCM Authentication Errors - Remove subscriber from the db
					PushSubscriber.remove({ "registration_id" : subscriber.registration_id })
						.then(function(sub){
							resolve();
						},
						function(err){
							reject(err);
						});
		      }
		      else {
				reject();
		      }
	    });

	    request.on("error", function(e) {
	      reject(e);
	    });    
	    // Send our payload 
	    request.write(encrypted.cipherText);
	    // End the request
	    request.end();
	});
};

/*==================================*/
/*====| POST Subscribers |====*/
/*==================================*/

/**
 * POST /api/subscribers
 * Will create a new subscriber in the db for push notifications
 */

PushNotifRouter.post('/subscribers', function(req, res){
    // Add to the new subscriber to the db
    PushSubscriber.create(req.body)
	    .then(function(subscriber){
	        res.json(subscriber);
	    }, function(err){
	    	res.status(400).send(err.message);
	    });
});

/**
 * DELETE /api/subscribers
 * Will delete a subscriber
 */

PushNotifRouter.delete('/subscribers/:registration_id', function(req, res){

   PushSubscriber.remove({ "registration_id" : req.params.registration_id })
	   .then(function(sub){
	   		res.status(200).send(sub);
	   },
	   function(err){
	   		res.status(400).send(err.message);
	   })
	   .catch(function(err){
	   		res.status(400).send(err.message);
	   });
});

/*===================================*/
/*==| CMS Push a new notification |==*/
/*===================================*/
/**
 * POST /api/notify
 * Will push the notification to all subscribers
 */

PushNotifRouter.post('/notify', function(req, res){
	// Get all subscribers from the db
	PushSubscriber.find({})
	  .then(function(subscribers){

	  	var payload = JSON.stringify(req.body);

	  	// Create an array of subscriber promises
	  	var subscribersProm = subscribers.map(function(sub){
			return pushToSubscriber(sub, payload);
	  	});

	  	Promise.all(subscribersProm)
		  	.then(function(value){
		  		// Resolve each subscription since we don't really have individual cb's 
				return Promise.resolve();
		  	}, 
		  	function(reason){
		  		// Reject if any come back, so that the last promise sends us the single reject
		  		return Promise.reject();
		  	})
		  	.then(function(){
		  		// All push notifs have successfully been sent
		  		res.status(200).send('Sent all notifications');
		  	},
		  	function(){
		  		// Catch all error
		  		res.status(400).send('There was an error with one of the subscriptions');
		  	});
	  },
	  function(err){
	    next(err);
	  })
	  .catch(function(err){
	  	next(err);
	  });
});

module.exports = PushNotifRouter;