var express = require('express'),
	app = express(),
  path = require('path'),
  api = require('./api/api'),
  mongoose = require('mongoose'),
  bodyParser = require('body-parser'),
  config = require('./config/config');

// Serve everything within /www as a static resource
app.use(express.static('www'));

// BodyParser makes it possible to post JSON to the server
// it makes it so we can access data we post as req.body
// parses application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parses application/json

/*==================================*/
/*========| Set up the DB |=========*/
/*==================================*/

mongoose.connect(config.db);
// As of Mongoose 4.1.0 built in promises are deprecated, Use ES6 Promises instead
mongoose.Promise = global.Promise;  

/*==================================*/
/*===========| API Routes |=========*/
/*==================================*/

app.use('/api', api);

/*==================================*/
/*=============| Views |============*/
/*==================================*/

app.get('/', function (req, res) {
   res.sendFile(path.resolve(__dirname + '/../www/views/index.html'), function(err){
      if(err){
          res.status(err.status).end();
      }
   });
});

// This is our fake/dummy CMS page
app.get('/cms', function (req, res) {
   res.sendFile(path.resolve(__dirname + '/../www/views/cms.html'), function(err){
      if(err){
          res.status(err.status).end();
      }
   });
});

/*==================================*/
/*====| Global Error Handling |=====*/
/*==================================*/
/* Create our error handling middleware */

app.use(function(err, req, res, next){
  if(err){
    var status = err.status || 500;
    res.status(status).send(err.message);
  }
});

// Export the app 
module.exports = app;