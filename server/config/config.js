var config = {
	env : process.env.NODE_ENV || 'development',
	port : process.env.NODE_PORT || 9999,
	db : process.env.MONGO_DB || 'mongodb://localhost/demopush',
	gcm : process.env.GCM_KEY || 'AIzaSyC5La61l2r5rMGQOEfu8MEvyyssQYlcLq8'
};

module.exports = config;