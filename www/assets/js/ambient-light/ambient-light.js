if(typeof window.DeviceLightEvent !== "undefined"){

	window.addEventListener('devicelight', function(event) {
		console.log(event.value);
		// Low Light
		if(event.value < 30){
			$('body').addClass('theme--dark');
		}
		else {
			$('body').removeClass('theme--dark');
		}
	});
}