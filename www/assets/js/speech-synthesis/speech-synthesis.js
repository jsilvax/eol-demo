(function(){
    
    if (typeof window.speechSynthesis === "undefined") {
        $('[data-hook="speak"]').hide();
        return;
    }

    // Cancel any previous speeches. 
    // ex. If you refresh the page while speech synthesis is speaking, it will continue as if no refersh had happened.
    // iOS doesnt seem to respect this cancel method
    speechSynthesis.cancel();

    var _speak = {
        hasSpoken : false, 
        speakArticle : function(){
             var speech = new SpeechSynthesisUtterance();
             speech.rate = 1; // A float value between 0 and 10
             speech.volume = 1; // A float value between 0 and 1
             speech.pitch = 1; // A float value between 0 and 2
             speech.lang = 'en-US';

             speech.text = $('[data-hook="article__title"]').text() + $('[data-hook="article__text"]').text();

             speech.onstart = function(event) {
                // If you want do something...
             }
             speech.onend = function(event) { 
                // If you want do something
             }

             speechSynthesis.speak(speech);
        },
        handleEvent : function(){
            $('[data-hook="speak"]').on('click', function(){
                if(this.hasSpoken === false){
                    this.hasSpoken = true;
                    this.speakArticle();
                    // Let user know they can pause
                    $('[data-hook="speak"]').text('Pause');
                }
                else if(speechSynthesis.speaking && speechSynthesis.paused === false){
                    speechSynthesis.pause();
                    // Let user know they can resume
                    $('[data-hook="speak"]').text('Resume');
                }
                else {
                    // Android Chrome doesn't respect this
                    speechSynthesis.resume();
                    // Let user know they can pause
                    $('[data-hook="speak"]').text('Pause');
                }
                 
            }.bind(this));
        }
    };

    _speak.handleEvent();

})();