(function(){

/*==========================*/
/*====| Click handlers |====*/
/*==========================*/

	var _btn = {
		subscribe : function(registration){
			// Hide unsubscribe btn
			$('[data-hook="push__unsubscribe"]')
				.css('display', 'none')
				.off('click');

			// Show push btn
			$('[data-hook="push__subscribe"]')
				.css('display', 'block')
				.on('click', function(){
					console.log('reg', registration);
					_push.subscribe(registration);
				});
		}, 
		unsubscribe : function(registration){
			// Hide subscribe btn
			$('[data-hook="push__subscribe"]')
				.css('display', 'none')
				.off('click');

			// Let a user unsubscribe
			$('[data-hook="push__unsubscribe"]')
				.css('display', 'block')
				.on('click', function(){
					_push.unsubscribe(registration);
				});
		}
	};

/*==========================*/
/*=========| Push |=========*/
/*==========================*/

	var _push = {
		updatePushStatus : function(subscription){
			// Get key and encode to base64
			var rawKey = subscription.getKey ? subscription.getKey('p256dh') : null;
			var key = rawKey ? btoa(String.fromCharCode.apply(null, new Uint8Array(rawKey))) : null;
			// Get auth and encode to base64
			var rawAuthSecret = subscription.getKey ? subscription.getKey('auth') : null;
			var auth = rawAuthSecret ? btoa(String.fromCharCode.apply(null, new Uint8Array(rawAuthSecret))) : null;
			// Figure out if we are on chrome
			var isChrome = subscription.endpoint.match(/^(http|https):\/\/?android.googleapis.com\/gcm\/send\//);
			var type = isChrome ? "chrome" : "webpush";
			// Get the registration id from the endpoint
            var id = subscription.endpoint.substr(subscription.endpoint.lastIndexOf('/') + 1);

            // Save the subscription to the db
            fetch('/api/subscribers', {
            	method : 'POST',
            	headers : {
            		'content-type' : 'application/json'
            	},
            	body : JSON.stringify({ 
            		"registration_id" : id,
            		"type" : type,
            		"endpoint" : subscription.endpoint,
            		"key" : key,
            		"auth" : auth
            	})
            })
            .then(function(response){
            	if(response.status !== 200){ return; }

				response.json().then(function(data) {  
					alert('Successfully Subscribed');
				});
            },
            function(err){
            	// TODO handle error
            });
		},
		subscribe : function(registration){
			// userVisibleOnly tells the browser that a notification will always be shown
		 	// when a push message is received
			registration.pushManager.subscribe({
	            userVisibleOnly: true
	        })
	        .then(function(subscription){
	        	// Save the subscription to the db
	        	this.updatePushStatus(subscription);
	        	// Show unsubscribe btn now
		        _btn.unsubscribe(registration);

	        }.bind(this),
	        function(err){
	        	// Ex. permission denied
				console.log('error', err);
	        });
		}, 
		unsubscribe : function(registration){
			var reg = registration;

			registration.pushManager.getSubscription().then(function(subscription) {
				subscription.unsubscribe()
				 .then(function(e) {
				    // Get the registration_id from the endpoint
				    var id = subscription.endpoint.substr(subscription.endpoint.lastIndexOf('/') + 1);
				    // Now that we unsubscribed we need to remove from db
					fetch('/api/subscribers/'+ id, {
		            	method : 'DELETE',
		            	headers : {
		            		'content-type' : 'application/json'
		            	}
		            })
		            .then(function(response){
		            	// On error do nothing
		            	if(response.status !== 200){ return; }

						response.json().then(function(data) {
							alert('Successfully unsubscribed');
							// Success! Show the subscribe btn again
							_btn.subscribe(reg);
						});
		            },
		            function(err){
		            	// TODO handle error
		            });

				 },
				 function(err){
				 	console.log('Error unsubscribing', err);
				 });
			});
		}

	};	

/*===================================*/
/*=====| Set up Service Worker |=====*/
/*===================================*/

if ('serviceWorker' in navigator) {
// Service worker is supported
 navigator.serviceWorker.register('/assets/js/push-notification/service-worker.js')
 	.then(function(registration){
        // Check the current Notification permission.
        if (Notification.permission === 'denied') {
            return;  
        }

 		// Check to see if we already have a subscription
		registration.pushManager.getSubscription().then(function(subscription) {
			if(subscription){
				// We have an existing subscription!
				// Always keep subscription up to date / synced with db 
				_push.updatePushStatus(subscription);

				// Allow the user to unsubscribe on click
				_btn.unsubscribe(registration);
			}
			else { 
				// Allow the user to subscribe on click
				_btn.subscribe(registration);
			}
		
		});
 	},
 	function(err){
 		console.log('Error', err);
 	});
}

})();