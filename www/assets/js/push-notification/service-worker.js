// In a service worker, "self" refers to the ServiceWorkerGlobalScope object

// Force the registered Service Worker to replace the existing one thats already installed,
// and replace the currently active worker on open pages.
self.addEventListener('install', function(e) {
  e.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', function(e) {
  e.waitUntil(self.clients.claim());
});

/*===================================*/
/*===| Notification Click Handler |==*/
/*===================================*/
// This event handles the pushing of the message received from the server

self.addEventListener('push', function(e) {
  console.log('Received a push message', e);
  var data = (typeof e.data !== "undefined") ? e.data.json() : {};
  var title = data.title || "E! Breaking News";  

  e.waitUntil(
    self.registration.showNotification(title, {
      body: data.message || 'Head to E! for breaking news', 
      icon: '/assets/img/e-logo.jpg',
      data : data.url || "http://eonline.com"
    })
  );
});

/*===================================*/
/*===| Notification Click Handler |==*/
/*===================================*/

self.addEventListener('notificationclick', function(e) {
    e.notification.close();
    var url = e.notification.data;
    e.waitUntil(
        clients.matchAll({
            type: 'window'
        })
        .then(function(windowClients) {
          // If the window is already open just focus on it
          windowClients.forEach(function(client){
              if (client.url === url && 'focus' in client) {
                  return client.focus();
              }
          });

          if (clients.openWindow) {
              return clients.openWindow(url);
          }
        })
    );
});