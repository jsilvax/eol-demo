Make sure you have node, npm, mongo installed

// 1. Download all dependencies from NPM, this creates your node_modules 
npm install 

// 2. Make sure mongo is running 
mongod

// 3. Start the server
npm run start

