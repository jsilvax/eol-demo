var app = require('./server/server');

// Listen to port
var server = app.listen(9999, function(){
  var port = server.address().port;
  console.log("Listening at", port)
});